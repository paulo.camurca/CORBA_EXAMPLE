#!/usr/bin/env python
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
sys.path.append('/usr/local/lib64/python2.7/site-packages/')

import CORBA
import Philosophy
import CosNaming


class PhilosophyClient(object):
    def __init__(self):
        sys.argv.extend(("-ORBInitRef", "NameService=corbaname::localhost"))
        orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)

        # get the naming service
        name_service_obj = orb.resolve_initial_references("NameService")
        name_service_root = name_service_obj._narrow(CosNaming.NamingContext)

        # set up the name of the method server
        method_name_server = [CosNaming.NameComponent("MyServer", "philoServer")]

        try:
            self.method = name_service_root.resolve(method_name_server)
        except CosNaming.NamingContext.NotFound:
            print "Could not find object"
            sys.exit(1)

    def philosophize(self):
        print self.method.philosophize()


if __name__ == "__main__":
    f = PhilosophyClient()
    f.philosophize()


#!/usr/bin/env python

import sys
import os
sys.path.append('/usr/local/lib/python2.7/site-packages')
sys.path.append('/usr/local/lib64/python2.7/site-packages')

from omniORB import CORBA, PortableServer
import CosNaming
import Philosophy
import Philosophy__POA

PHILOSOPHY_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), 'philosophy.py'))


class PhilosophyServer(Philosophy__POA.PhilosophyServer):
    def philosophize(self):
        pipe = os.popen(PHILOSOPHY_PATH)
        wisdom = pipe.read()
        if pipe.close():
            # An error occurred with the pipe
            wisdom = "An error occurred while trying to philosophize."
        return wisdom


sys.argv.extend(("-ORBInitRef", "NameService=corbaname::localhost"))

orb = CORBA.ORB_init(sys.argv, CORBA.ORB_ID)
poa = orb.resolve_initial_references("RootPOA")

servant = PhilosophyServer()
# poa.activate_object(servant)
eo = servant._this()

obj = orb.resolve_initial_references("NameService")
rootContext = obj._narrow(CosNaming.NamingContext)

method_name = [CosNaming.NameComponent("MyServer", "philoServer")]

try:
    rootContext.bind(method_name, eo)
    print "Bound the philosophize to the naming service."
except CosNaming.NamingContext.AlreadyBound, msg:
    print "philosophize already bound, rebinding the new object."
    rootContext.rebind(method_name, eo)

poa._get_the_POAManager().activate()
print "Servidor iniciado!"
orb.run()
